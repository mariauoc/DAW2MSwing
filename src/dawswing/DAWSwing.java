/*
 * Ejemplo de programa con entorno gráfico
 */
package dawswing;

/**
 *
 * @author DAM
 */
public class DAWSwing {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Creamos el JFrame y lo mostramos
        // Instanciamos un objeto Principal
        Principal principal = new Principal();
        // Centramos la ventana en la pantalla
        principal.setLocationRelativeTo(null);
        // Lo hacemos visible
        principal.setVisible(true);
        
    }
    
}
